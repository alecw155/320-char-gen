# Overview

This repository is to create a software that is able to automate a Dungeons and Dragons 5e character creator made with Javascript and HTML. The program will allow the user to choose their character attributes, race, class, background, biography, allighnment, skills, features, spells and inventory. The user will be able to export the character as printable character sheet or as a JSON file.

# Authors

    Alec Williams   alec.b.williams@wsu.edu
    Austin Reyes    austin.reyes@wsu.edu
    Nathaniel Adams nathaniel.b.adams@wsu.edu
    Trevor Holland  trevor.holland@wsu.edu

## Table of Contents

- [Main](#Main)
- [Background](#background)
- [Roadmap](#roadmap)
    - [To be done](#not started)
    - [Inprogress](#inprogress)
    - [Completed](#completed)
    - [Extra](#extra)
- [Install](#install)
- [Usage](#usage)
- [Appendex](#appendex)

## Main

this is a test

## Background


## Roadmap

This is a list of items that we are working on or need to finish.

### Not Started
- [ ] Create a creator Screen
- [ ] Add options for USer
- [ ] Submit the character options
- [ ] Be able to print the character
- [ ] Make a recovery options
### Inprogress
- [ ] Mobile Support
### Completed
- [X] Create a navbar
- [X] Drop Down menu for subpages
- [X] Mobile Support for the navbar
- [X] Webpages to Navigate
- [X] Incorperate Home Screen
- [X] Create an about section
### Extra

## Install

For to compile and us the run.bat you must install all correct packages for it to work.

Please use the following commands in the current directory.
- npm install create-react-app
- npm install --save react-router-dom
- npm install react-transition-group
- npm install react-expand-animated
- npm install antd
- npm install --save-dev mocha
- npm install --save-dev chai
- npm install --save-dev jsdom
- npm install -save-dev @babel/register

## Usage



## Appendex 
