import './App.css';
import NavBar from "./components/NavBar/NavBar";
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import Home from './components/pages/Home';
import Creator from './components/pages/Creator';
import About from './components/pages/About';
import New_char from './components/pages/New_char';
import Recov_char from './components/pages/Recov_char';
import Print_char from './components/pages/Print_char';

function App() {
  return (
    <Router>
      <NavBar />
      <Switch>
        <Route path='/' exact component={Home} />
        <Route path='/creator' component={Creator}/>
        <Route path='/about' component={About}/>
        <Route path='/new_char' component={New_char}/>
        <Route path='/recov_char' component={Recov_char}/>
        <Route path='/print_char' component={Print_char}/>
      </Switch>
    </Router>
  );
}

export default App;
