import React from 'react';

function AttributeInput(props){
  let inputStyle = {
    ...(props.readOnly && {border: "0"})
  }

  //TODO: add a section to the right that displays points spend & formula
  return (
    <div className="attribute-input">
      <input 
        className={props.error}
        type="number" 
        name={props.name}
        value={props.value} 
        onChange={(event) => props.onClick(parseInt(event.target.value))}
        readOnly={props.readOnly}
        style={inputStyle}
      />
      <div className="attribute-buttons">
        <button onClick={() => {
          if (props.value < 15)
            props.onClick(1)
          }}>+</button>
        <button onClick={() => {
          if (props.value >= 4)
            props.onClick(-1)}
        }>-</button>
      </div>
    </div>
  );
}

export default AttributeInput;