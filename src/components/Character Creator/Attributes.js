import React from 'react';
import AttributeInput from './AttributeInput';
import { Stats, RaceFeatures, getSign, calculateStatBonus } from './Util';

function Attributes(props) {
  function renderAttributeInput(stat) {
    let raceBonus = RaceFeatures[props.race] ? RaceFeatures[props.race].statChanges[stat] : 0;
    return (
      <div className="attribute-input-container">
        <div class="attribute-total">
          Total: {props.value[stat] + raceBonus} ({getSign(calculateStatBonus(props.value[stat] + raceBonus)) + calculateStatBonus(props.value[stat] + raceBonus)})
        </div>
        <AttributeInput
          name={stat}
          onClick={(value) => props.update(stat, value)}
          value={props.value[stat]} />
          Race Adjust: {getSign(raceBonus) + raceBonus}
        <h3>{stat}</h3>
      </div>
    )
  };

  return (
    <div className="attributes">
      <div className="attribute-wrapper">
        <div className="attribute-flex">
          {renderAttributeInput(Stats.STR)}
          {renderAttributeInput(Stats.DEX)}
          {renderAttributeInput(Stats.CON)}
        </div>
        <div className="attribute-flex">
          {renderAttributeInput(Stats.INT)}
          {renderAttributeInput(Stats.WIS)}
          {renderAttributeInput(Stats.CHA)}
        </div>
      </div>
    </div>
  );
};

export default Attributes;