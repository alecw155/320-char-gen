import React from 'react';
import {Backgrounds} from './Util';

function Background(props) {
  const backgroundList = [];

  for (var background_ in Backgrounds) {
    backgroundList.push(
        <option value={background_}>{background_}</option>
    )
  }

  return (
    <select name="background" id="background_select"
            onChange={(event) => props.update(event.target.value)}>
      <option value="blank">---</option>
      {backgroundList}
    </select>
  )
}

export default Background;