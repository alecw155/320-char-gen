import React from 'react';
import {calculateStatBonus, RaceFeatures, ClassFeatures, Stats, calculateHitPoints, EquipmentList, Classes, Backgrounds, BackgroundFeatures} from './Util';
import Skill from './Skill';
import Race from './Race';
import Class from './Class';
import ClassSkills from './ClassSkills';
import Background from './Background';
import Equipment from './Equipment';
import Expander from '../Expander';
import Attributes from './Attributes';
import Shop from './Shop';
import './Character.css';
import testUtils from 'react-dom/test-utils';

class Character extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      name: "",
      race: "",
      class_: "",
      level: 1,
      background: "",
      attributes: {
        [Stats.STR]: 10,
        [Stats.DEX]: 10,
        [Stats.CON]: 10,
        [Stats.INT]: 10,
        [Stats.WIS]: 10,
        [Stats.CHA]: 10
      },
      classSkills: [],
      backgroundSkills: [],
      hitPoints: 1,
      armorClass: 10,
      speed: 30,
      saves: [],
      equipment: [],
      spells: [],
      gold: 4,
    }

    this.changeName = this.changeName.bind(this);
    this.changeAttributes = this.changeAttributes.bind(this);
    this.changeRace = this.changeRace.bind(this);
    this.changeClass = this.changeClass.bind(this);
    this.changeClassSkills = this.changeClassSkills.bind(this);
	  this.changeBackground = this.changeBackground.bind(this);
    this.changeEquipment = this.changeEquipment.bind(this);
	  this.changeGold = this.changeGold.bind(this);
    this.retrieveLocalStorage();
  }

  changeName = (event) => {
    this.setState({name: event.target.value});
    this.setLocalStorage();
  }

  changeAttributes = (attribute, value) => {
    const attributes = this.state.attributes;
    attributes[attribute] = attributes[attribute] + value;
    this.setState({attributes: attributes});
    this.setLocalStorage();
  }

  changeRace = (newRace) => {
    if (newRace == "blank")
      return;

    this.setState ({race: newRace});
    this.setLocalStorage();
  }

  changeClass = (newClass) => {
    if (newClass == "blank")
      return;

    this.setState({class_: newClass});
    this.setState({equipment: ClassFeatures[Classes[newClass]].equipment})
    console.log("setting new equipment: " + this.state.equipment)
    this.setState({gold: ClassFeatures[Classes[newClass]].gold})
    this.setLocalStorage();
  }

  changeClassSkills = (newSkills) => {
    this.setState({classSkills: newSkills})
    this.setLocalStorage();
  }

  changeBackground = (newBackground) => {
    if (newBackground == "blank")
      return;

    this.setState({background: newBackground});

    console.log("BackgroundFeatures[newBackground]")
    console.log(BackgroundFeatures[newBackground])

    if (BackgroundFeatures[Backgrounds[newBackground]]){
      console.log("setting background equipment")
      var list = Array.from(this.state.equipment);
      var newList = list.concat(BackgroundFeatures[Backgrounds[newBackground]].equipment);
      this.setState({equipment: newList})
    }
    //be sure to set new background skills from Util.js
    this.setLocalStorage();
  }

  changeEquipment = (newEquipment) => {
    this.setState({equipment: newEquipment});
    this.setLocalStorage();
  }

  addEquipment = (newEquipment) => {
    var list = Array.from(this.state.equipment);
    var newList = list.concat(newEquipment);
    this.setState({equipment: newList})
    this.setLocalStorage();
  }

  changeGold = (newGold) => {
    this.setState({gold: newGold});
    this.setLocalStorage();
  }

  retrieveLocalStorage = () => {
    const localData = localStorage.getItem('character');
    if (localData ? this.state = JSON.parse(localData) : false );
  }

  // will save all changes to local storage
  setLocalStorage = () => {
    localStorage.setItem('character', JSON.stringify(this.state));
  }

  render() {
    let hp = this.state.class_ != "" ? calculateHitPoints(this.state.class_, this.state.race, this.state.attributes[Stats.CON]) : "";

    let size = RaceFeatures[this.state.race] ? RaceFeatures[this.state.race].size : "";
    let speed = RaceFeatures[this.state.race] ? RaceFeatures[this.state.race].speed : "";

    var classSkills = [];
    var backgroundSkills = [];
    var allSkills = [];

    if (this.state.classSkills)
      classSkills = Array.from(this.state.classSkills);
    if (this.state.backgroundSkills)
      backgroundSkills = Array.from(this.state.backgroundSkills);
    if (classSkills && backgroundSkills)
      allSkills = classSkills.concat(backgroundSkills)

    return (
      <div className="test">
        <div className="char-info">
          <div className="misc-info">
            <div className="name">
              Name: <input id="name-input" type="text" value={this.state.name} onChange={this.changeName}/>
            </div>
            Level 1 {this.state.race} {this.state.class_}<br></br>
            HP: {hp}<br></br>
            Size: {size}<br></br>
            Speed: {speed}<br></br>
          </div>
          <Skill 
            skills={allSkills}
            attributes={this.state.attributes}
            race={this.state.race}
            class_={this.state.class_}/>
        </div>
        <Expander 
          title="Attributes"
          open="true">
          <Attributes 
            update={(i, v) => this.changeAttributes(i, v)} 
            race={this.state.race}
            value={this.state.attributes}/>
        </Expander>
        <Expander title="Race">
          Race: {this.state.race}<br></br>
          <Race update={(v) => this.changeRace(v)}/> <br></br>
        </Expander>
        <Expander title="Class">
          Class: {this.state.class_}<br></br>
          <Class update={(v) => this.changeClass(v)}/> <br></br>
          <ClassSkills 
            class={this.state.class_}
            update={(v) => this.changeClassSkills(v)}/>
        </Expander>
		    <Expander title="Background">
          Background: {this.state.background}<br></br>
        <Background update={(v) => this.changeBackground(v)}/> <br></br>
        </Expander>
        <Expander title="Equipment">
          <Equipment 
            update={(v) => this.changeEquipment(v)}
            inventory={this.state.equipment}/>
          <Expander title="Shop" id="expander-shop">
            Current Wealth: {" "}
            <span className="plat">Pp: {Math.trunc(this.state.gold)} {" "}</span>
            <span className="gold">Gp: {Math.trunc((this.state.gold-Math.trunc(this.state.gold))*10)} {" "}</span>
            <span className="silver">Sp: {Math.trunc((this.state.gold*10-Math.trunc(this.state.gold*10))*10)} {" "}</span>
            <span className="copper">Cp: {Math.trunc((this.state.gold*100-Math.trunc(this.state.gold*100))*10)}</span>
            <Shop 
              gold={this.state.gold}
              changeGold={(v) => this.changeGold(v)}
              addEquipment={(v) => this.addEquipment(v)}/>
          </Expander>
        </Expander>
      </div>
    );
  }
}

export default Character;