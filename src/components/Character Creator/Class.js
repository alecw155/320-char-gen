import React from 'react';
import {Classes} from './Util';

function Class(props) {
  const classList = [];

  for (let class_ in Classes) {
    classList.push(
        <option value={class_}>{class_}</option>
    )
  }

  return (
    <select name="class_" id="class_select"
      onChange={(event) => props.update(event.target.value)}>
      <option value="blank">---</option>
      {classList}
    </select>
  )
}

export default Class;