import React from 'react';
import { Classes, ClassFeatures, Skills } from './Util';
import {Checkbox, Col} from 'antd';

class ClassSkills extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      checked: [],
      class: props.class,
      limit: 0,
    }

    let incomingProps = props;

    this.renderSkillSelect = this.renderSkillSelect.bind(this);
    this.onChange = this.onChange.bind(this);
    this.isDisabled = this.isDisabled.bind(this);
  }

  renderSkillSelect = (s) => {
    return (
      <Checkbox value={Skills[s]} name={Skills[s]}>
        {Skills[s]}
      </Checkbox>
    );
  }

  onChange = (values) => {

  }

  isDisabled = (skill) => {

  }

  render() {
    var skillSelect = [];

    if (ClassFeatures[this.state.class]) {
      //console.log("class skills for " + this.state.class + ": ")
      //console.log(ClassFeatures[Classes[this.state.class]].skills.list)

      for (var s in ClassFeatures[Classes[this.state.class]].skills.list) {
        skillSelect.push(
          <div>
            {this.renderSkillSelect(s)}
          </div>
        )
      }
    }
      
    return (
      <div className="classSkills">
        <Checkbox.Group onChange={(v) => this.onChange(v)}>
          <Col >
            {/*skillSelect*/}
          </Col>
        </Checkbox.Group>
      </div>
    );
  }
  
}

export default ClassSkills;