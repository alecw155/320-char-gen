import React from 'react';
import {EquipmentList} from './Util';

function Equipment(props) {
  const equipmentList = [];

  for (var item in props.inventory) {
    let currentItem = item;

    equipmentList.push(
        <div className="equipment-item">
          {props.inventory[item]}
          <button onClick={() => removeItem(props.inventory[currentItem])}>X</button> 
        </div>
    )
  }


  function removeItem(item) {
    const list = props.inventory
    const index = list.lastIndexOf(item);
    
    if (index > -1) {
      list.splice(index, 1);
    }
    props.update(list)
  }


  return (
      <div className="equipment">
        {equipmentList}
      </div>
  )
}

export default Equipment;