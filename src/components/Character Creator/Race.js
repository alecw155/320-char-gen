import React from 'react';
import {Races} from './Util';

function Race(props) {
  const raceList = [];

  for (var race in Races) {
    raceList.push(
      <option value={Races[race]}>{Races[race]}</option>
    )
  }

  return (
  <select name="race" id="race_select" 
    onChange={(event) => props.update(event.target.value)}>
    <option value="blank">---</option>
    {raceList}
  </select>
  )
}

export default Race;