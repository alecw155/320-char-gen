import React from 'react';
import ShopInput from './ShopInput'
import { EquipmentList, EquipmentCosts } from './Util';

class Shop extends React.Component{
  constructor(props) {
    super(props);
    this.state = {
      purchaseList: [],
      gold: props.gold,
      props: props
    };

    this.removeAllItems = this.removeAllItems.bind(this);
    this.renderShopItem = this.renderShopItem.bind(this);
    this.changeList = this.changeList.bind(this);
    this.tallyPurchase = this.tallyPurchase.bind(this);
    this.getTotal = this.getTotal.bind(this);
  }

  removeAllItems = (item, list) => {
    var i = 0;

    while (i < list.length) {
      if (list[i] === item) {
        list.splice(i, 1);
      } else {
        i++
      }
    }

    return list;
  }

  changeList = (item, qty) => {
    var list = this.state.purchaseList

    list = this.removeAllItems(item, list)

    for (let i = 0; i < qty; i++) {
      list.push(item)
    }

    this.setState({purchaseList: list});
    console.log(this.state.purchaseList)
  }

  renderShopItem = (item) => {
    return (
        <div className="shop-item">
          <div className="shop-item-qty">
            <ShopInput
                item={item}
                update={(v) => this.changeList(item, v)}/>
          </div>
          <div className="shop-item-name">
            {EquipmentList[item]}
          </div>
          <div className="shop-item-value">
            {EquipmentCosts[EquipmentList[item]]}
          </div>
        </div>
    );
  }

  getTotal = () => {
    var total = 0;

    for(let purchase in this.state.purchaseList) {
      total += EquipmentCosts[EquipmentList[this.state.purchaseList[purchase]]];
    }

    return total;
  }

  tallyPurchase = () => {
    var total = this.getTotal();

    if (total <= this.state.gold) {
      this.props.addEquipment(this.state.purchaseList);
      this.props.changeGold(this.state.gold - total);
    }
  }

  render() {
    const itemList = [];

    for (var item in EquipmentList) {
      itemList.push(this.renderShopItem(item))
    }

  return(
      <div className="shop">
        Purchase Price: <span className="plat">Pp: {Math.trunc(this.getTotal())} {" "}</span>
			  <span className="gold">Gp: {Math.trunc((this.getTotal()-Math.trunc(this.getTotal()))*10)} {" "}</span>
			  <span className="silver">Sp: {Math.trunc((this.getTotal()*10-Math.trunc(this.getTotal()*10))*10)} {" "}</span>
			  <span className="copper">Cp: {Math.trunc((this.getTotal()*100-Math.trunc(this.getTotal()*100))*10)}</span><br></br>
        <button onClick={() => this.tallyPurchase()}>
          PURCHASE
        </button>
        <div className="shop-titles">
          <div className="shop-item">
            <div className="shop-item-qty">
              Qty.
            </div>
            <div className="shop-item-name">
              Name
            </div>
            <div className="shop-item-value">
              Cost
            </div>
          </div>
        </div>
        <div className="items-list">
          {itemList}
        </div>
      </div>
    )
  }
}

export default Shop;