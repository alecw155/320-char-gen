import React from 'react';
import { RaceFeatures } from './Util';

class ShopInput extends React.Component{
  constructor(props){
    super(props)
    this.state = {
      value: 0,
    }

    this.changeQty = this.changeQty.bind(this);
  }

  changeQty = (value) => {
    this.setState({value:value})
    this.props.update(value)
  }

  render() {
    return (
      <div className="shop-input">
        <button onClick={() => {
          this.changeQty(this.state.value+1)
        }}>+</button>
        <input 
          type="number" 
          value={this.state.value}
        /> 
        <button onClick={() => {
          this.changeQty(this.state.value-1)
        }}>-</button>
      </div>
    );
  }
}

export default ShopInput;