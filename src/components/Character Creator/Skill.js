import React from 'react';
import { calculateStatBonus, ClassFeatures, RaceFeatures, Skills, SkillStats } from './Util';

function Skill(props) {
  const skillList = []
  var statBonus = 0;
  var profBonus = 0;
  // stat bonus + prof bonus 

  for (var s in Skills) {
    var propsSkills = Array.from(props.skills);
    var raceStatAdjustment = props.race != "" ? RaceFeatures[props.race].statChanges[SkillStats[Skills[s]]] : 0;
    statBonus = calculateStatBonus(props.attributes[SkillStats[Skills[s]]] + raceStatAdjustment);
    if (props.class_ != "")
      profBonus = propsSkills.includes(s) ? ClassFeatures[props.class_].proficiency : 0;
    else
      profBonus = 0;
//  
    
    skillList.push(
      <div className="skill-wrapper">
        <div className="skill-total">
          +{statBonus + profBonus}
        </div>
        <div className="skill-name">
          {Skills[s]}
        </div>
        <div className="skill-prof">
        </div>
        <div className="skill-stat">
        </div>
      </div>
    );
  }
  
  return(
    <div className="skill-wrappers">
      {skillList}
    </div>
  );
}

export default Skill;

