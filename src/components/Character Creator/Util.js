import Race from "./Race"

export const Sizes = {
  Small: "Small",
  Medium: "Medium",
  Large: "Large",
}

export const EquipmentList = {
  Club: "Club",
  Dagger: "Dagger",
  Handaxe: "Hand Axe",
  Javelin: "Javelin",
  Light_Hammer: "Light Hammer",
  Mace: "Mace",
  Quarterstaff: "Quarterstaff",
  Sickle: "Sickle",
  Spear: "Spear",

  Light_Crossbow: "Light Crossbow",
  Dart: "Dart",
  Shortbow: "Shortbow",
  Sling: "Sling",

  Battleaxe: "Battleaxe",
  Flail: "Flail",
  Glaive: "Glaive",
  Greataxe: "Great Axe",
  Greatsword: "Greatsword",
  Halberd: "Halberd",
  Lance: "Lance",
  Longsword: "Longsword",
  Maul: "Maul",
  Morning_Star: "Morning Star",
  Pike: "Pike",
  Rapier: "Rapier",
  Scimitar: "Scimitar",
  shortsword: "Shortsword",
  Trident: "Trident",
  War_Pick: "War Pick",
  War_Hammer: "War-Hammer",
  Whip: "Whip",

  Blowgun: "Blowgun",
  Hand_Crossbow: "Hand Crossbow",
  Heavy_Crossbow: "Heavy Crossbow",
  Longbow: "Longbow",
  Net: "Net",

  Explorers_Pack: "Explorer's Pack",
  Diplomats_Pack: "Diplomat's Pack",
  Entertainers_Pack: "Entertainer's Pack",
  Priests_Pack: "Priest's Pack",
  Dungeoneers_Pack: "Dungeoneer's Pack",
  Burglars_Pack: "Burglar's Pack",
  Scholars_Pack: "Scholar's Pack",

  Bagpipes: "Bagpipes",
  Drum: "Drum",
  Dulcimer: "Dulcimer",
  Flute: "Flute",
  Horn: "Horn",
  Lute: "Lute",
  Lyre: "Lyre",
  Pan_Flute: "Pan flute",
  Shawm: "Shawm",
  Viol: "Viol",

  Leather_Armor: "Leather armor",
  Scale_Mail: "Scale mail",
  Chain_Mail: "Chain mail",
  Shield: "Shield",

  Crossbow_Bolts: "20 Crossbow Bolts",
  Arrows: "20 Arrows",

  Holy_Symbol: "Holy Symbol",
  Druidic_Focus: "Druidic Focus",
  Component_Pouch: "Component Pouch",
  Arcane_Focus: "Arcane Focus",

  Alchemists_Supplies: "Alchemist's Supplies",
  Brewers_Supplies: "Brewer's Supplies",
  Calligraphers_Tools: "Calligrapher's Tools",
  Carpenters_Tools: "Carpenter's Tools",
  Cobblers_Tools: "Cobbler's Tools",
  Cooks_Utensils: "Cook's Utensils",
  Glassblowers_Tools: "Glassblower's Tools",
  Jewelers_Tools: "Jeweler's Tools",
  Leatherworkers_Tools: "Leatherworker's Tools",
  Masons_Tools: "Mason's Tools",
  Painters_Supplies: "Painter's Supplies",
  Potters_tools: "Potter's Tools",
  Smiths_Tools: "Smith's Tools",
  Tinkers_Tools: "Tinker's Tools",
  Weavers_Tools: "Weaver's Tools",
  Woodcarvers_Tools: "Woodcarvers Tools",

  Forgery_Kit: "Forgery Kit",
  Disguise_Kit: "Disguise Kit",
  Thieves_Tools: "Thieves' Tools",
  Herbalism_Kit: "Herbalism Kit",
  Navigators_Tools: "Navigator's Tools",

  Spellbook: "Spellbook",

  Prayer_Book: "Prayer Book",
  Prayer_Wheel: "Prayer Wheel",
  Incense: "5 Sticks of Incense",
  Vestments: "Vestments",
  Common_Clothes: "Common Clothes",
  Belt_Pouch: "Belt Pouch",
  Fine_Clothes: "Fine Clothes",
  Bottles_of_Liquid: "10 Stopped Bottles Filled With Colored Liquid",
  Weighted_Dice: "Set of Weighted Dice",
  Marked_Cards: "A Deck of Marked Cards",
  Signet_Ring: "Signet Ring",
  Fake_Signet_Ring: "Fake Signet Ring",
  Crowbar: "Crowbar",
  Dark_clothes: "Hooded Dark Common Clothes",
  Trinket: "Trinket",
  Costume: "Costume",
  Shovel: "Shovel",
  Iron_Pot: "Iron Pot",
  Mule: "Mule",
  Cart: "Cart",
  Scroll_Case: "Scroll case",
  Winter_Blanket: "Winter blanket",
  Purse: "Purse",
  Knights_Banner: "Knight's Banner",
  Staff: "Staff",
  Hunting_Trap: "Hunting Trap",
  bottle_of_Ink: "Bottle of Ink",
  Quill: "Quill",
  Small_Knife: "Small Knife",
  Silk_Rope: "Silk Rope",
  Lucky_Charm: "Lucky Charm",
  Rank_insignia: "Rank Insignia",
  Bone_dice: "Bone Dice",
  Deck_of_Cards: "Deck of Cards",
  Home_City_Map: "Home City, Map",
  Pet_Mouse: "Pet Mouse"
}
export const FeatureList = {
  Sherlter_the_Faithful: 'shelter the faithful',
  Criminal_Specialty: 'criminal specialty',
  Criminal_Contact: 'criminla contact',
  Favorite_Schemes: 'favorite scheme',
  False_Identiy: 'false identiy',
  Entertainer_Routines: 'entertainer routines',
  By_Popular_Demand: 'by popular demand',
  Defining_Event: 'defining event',
  Rustic_Hospitality: 'rustic hospitality',
  Guild_Bussiness: 'guild bussiness',
  Guild_Membership: 'guild membership',
  Life_of_Seclusion: 'life of seclusion',
  Discovery: 'Discovery',
  Retainers: 'retainers',
  Position_of_Privilege: 'position of privilage',
  Origin: 'origin',
  Wanderer: 'wanderer',
  Ships_Passage: "ship's passage",
  Bad_Reputation: 'bad reputation',
  Specialty: 'specialty',
  Researcher: 'researcher',
  City_Secrets: 'city secrets',
  
  Unarmored_Defense: 'unarmored defense',
  Rage: 'rage',
  Bardic_Inspiration: 'bardic inspiration',
  Druidic: 'druidic',
  Divine_Domain: 'divine_domain',
  Second_Wind: 'second wind',
  Fighting_Style: 'fighting style',
  Martial_Arts: 'martial arts',
  Divine_Sense: 'divine sense',
  Lay_on_Hands: 'lay on hands',
  Favored_Enemy: 'favored enemy',
  Natural_Explorer: 'natural explorer',
  Expertise: 'expertise',
  Sneak_Attack: 'sneak attack',
  Sorcerous_Origin: 'sorcerous origin',
  Otherworldly_Patron: 'otherworldly patron',
  Spellcasting: 'spellcasting',
  Pact_Magic: 'pact magic'
}

export const EquipmentCosts = {
  [EquipmentList.Club]: 0.01,
  [EquipmentList.Dagger]: 0.2,
  [EquipmentList.Handaxe]: 0.5,
  [EquipmentList.Javelin]: 0.05,
  [EquipmentList.Light_Hammer]: 0.2,
  [EquipmentList.Mace]: 0.5,
  [EquipmentList.Quarterstaff]: 0.02,
  [EquipmentList.Sickle]: 0.1,
  [EquipmentList.Spear]: 0.1,

  [EquipmentList.Light_Crossbow]: 2.5,
  [EquipmentList.Dart]: 0.005,
  [EquipmentList.Shortbow]: 2.5,
  [EquipmentList.Sling]: 0.01,

  [EquipmentList.Battleaxe]: 1.0,
  [EquipmentList.Flail]: 1.0,
  [EquipmentList.Glaive]: 2.0,
  [EquipmentList.Greataxe]: 3.0,
  [EquipmentList.Greatsword]: 5.0,
  [EquipmentList.Halberd]: 2.0,
  [EquipmentList.Lance]: 1.0,
  [EquipmentList.Longsword]: 1.5,
  [EquipmentList.Maul]: 1.0,
  [EquipmentList.Morning_Star]: 1.5,
  [EquipmentList.Pike]: 0.5,
  [EquipmentList.Rapier]: 2.5,
  [EquipmentList.Scimitar]: 2.5,
  [EquipmentList.shortsword]: 1.0,
  [EquipmentList.Trident]: 0.5,
  [EquipmentList.War_Pick]: 0.5,
  [EquipmentList.War_Hammer]: 1.5,
  [EquipmentList.Whip]: 0.2,

  [EquipmentList.Blowgun]: 1.0,
  [EquipmentList.Hand_Crossbow]: 7.5,
  [EquipmentList.Heavy_Crossbow]: 5.0,
  [EquipmentList.Longbow]: 5.0,
  [EquipmentList.Net]: 0.1,

  [EquipmentList.Explorers_Pack]: 1.0,
  [EquipmentList.Diplomats_Pack]: 3.9,
  [EquipmentList.Entertainers_Pack]: 4.0,
  [EquipmentList.Priests_Pack]: 1.9,
  [EquipmentList.Dungeoneers_Pack]: 1.2,
  [EquipmentList.Burglars_Pack]: 1.6,
  [EquipmentList.Scholars_Pack]: 4.0,

  [EquipmentList.Bagpipes]: 3.0,
  [EquipmentList.Drum]: 0.6,
  [EquipmentList.Dulcimer]: 2.5,
  [EquipmentList.Flute]: 0.2,
  [EquipmentList.Horn]: 0.3,
  [EquipmentList.Lute]: 3.5,
  [EquipmentList.Lyre]: 3.0,
  [EquipmentList.Pan_Flute]: 1.2,
  [EquipmentList.Shawm]: 0.2,
  [EquipmentList.Viol]: 3.0,

  [EquipmentList.Leather_Armor]: 1.0,
  [EquipmentList.Scale_Mail]: 5.0,
  [EquipmentList.Chain_Mail]: 7.5,
  [EquipmentList.Shield]: 1.0,

  [EquipmentList.Crossbow_Bolts]: 0.1,
  [EquipmentList.Arrows]: 0.1,

  [EquipmentList.Holy_Symbol]: 0.5,
  [EquipmentList.Druidic_Focus]: 0.5,
  [EquipmentList.Component_Pouch]: 2.5,
  [EquipmentList.Arcane_Focus]: 1.1,

  [EquipmentList.Alchemists_Supplies]: 5.0,
  [EquipmentList.Brewers_Supplies]: 2.0,
  [EquipmentList.Calligraphers_Tools]: 1.5,
  [EquipmentList.Carpenters_Tools]: 0.8,
  [EquipmentList.Cobblers_Tools]: 0.5,
  [EquipmentList.Cooks_Utensils]: 0.1,
  [EquipmentList.Glassblowers_Tools]: 3.0,
  [EquipmentList.Jewelers_Tools]: 2.5,
  [EquipmentList.Leatherworkers_Tools]: 0.5,
  [EquipmentList.Masons_Tools]: 1.0,
  [EquipmentList.Painters_Supplies]: 1.0,
  [EquipmentList.Potters_tools]: 1.0,
  [EquipmentList.Smiths_Tools]: 2.0,
  [EquipmentList.Tinkers_Tools]: 5.0,
  [EquipmentList.Weavers_Tools]: 0.1,
  [EquipmentList.Woodcarvers_Tools]: 0.1,

  [EquipmentList.Forgery_Kit]: 1.5,
  [EquipmentList.Disguise_Kit]: 2.5,
  [EquipmentList.Thieves_Tools]: 2.5,
  [EquipmentList.Herbalism_Kit]: 0.5,
  [EquipmentList.Navigators_Tools]: 2.5,

  [EquipmentList.Spellbook]: 5.0,

  [EquipmentList.Prayer_Book]: 2.5,
  [EquipmentList.Prayer_Wheel]: 2.0,
  [EquipmentList.Incense]: 0.05,
  [EquipmentList.Vestments]: 0.3,
  [EquipmentList.Common_Clothes]: 0.05,
  [EquipmentList.Belt_Pouch]: 0.05,
  [EquipmentList.Fine_Clothes]: 1.5,
  [EquipmentList.Bottles_of_Liquid]: 2.0,
  [EquipmentList.Weighted_Dice]: 0.01,
  [EquipmentList.Marked_Cards]: 0.05,
  [EquipmentList.Signet_Ring]: 5.0,
  [EquipmentList.Fake_Signet_Ring]: 0.5,
  [EquipmentList.Crowbar]: 0.2,
  [EquipmentList.Dark_clothes]: 0.5,
  [EquipmentList.Trinket]: 0.1,
  [EquipmentList.Costume]: 0.5,
  [EquipmentList.Shovel]: 0.2,
  [EquipmentList.Iron_Pot]: 0.2,
  [EquipmentList.Mule]: 0.8,
  [EquipmentList.Cart]: 1.5,
  [EquipmentList.Scroll_Case]: 0.1,
  [EquipmentList.Winter_Blanket]: 0.05,
  [EquipmentList.Purse]: 0.1,
  [EquipmentList.Knights_Banner]: 0.2,
  [EquipmentList.Staff]: 0.5,
  [EquipmentList.Hunting_Trap]: 0.5,
  [EquipmentList.bottle_of_Ink]: 1.0,
  [EquipmentList.Quill]: 0.002,
  [EquipmentList.Small_Knife]: 0.5,
  [EquipmentList.Silk_Rope]: 1.0,
  [EquipmentList.Lucky_Charm]: 0.5,
  [EquipmentList.Rank_insignia]: 0.5,
  [EquipmentList.Bone_dice]: 0.01,
  [EquipmentList.Deck_of_Cards]: 0.05,
  [EquipmentList.Home_City_Map]: 0.1,
  [EquipmentList.Pet_Mouse]: 0.1
}


export const Stats = {
  STR: "Strength",
  DEX: "Dexterity",
  CON: "Constitution",
  INT: "Intelligence",
  WIS: "Wisdom",
  CHA: "Charisma"
}

export const Skills = {
  Acrobatics: "Acrobatics",
  AnimalHandling: 'Animal Handling',
  Arcana: "Arcana",
  Athletics: "Athletics",
  Deception: "Deception",
  History: "History",
  Insight: "Insight",
  Intimidation: "Intimidation",
  Investigation: "Investigation",
  Medicine: "Medicine",
  Nature: "Nature",
  Perception: "Perception",
  Performance: "Performance",
  Persuasion: "Persuasion",
  Religion: "Religion",
  SleightOfHand: 'Sleight of Hand',
  Stealth: "Stealth",
  Survival: "Survival"
}

export const SkillStats = {
  [Skills.Acrobatics]: Stats.DEX,
  [Skills.AnimalHandling]: Stats.WIS,
  [Skills.Arcana]: Stats.INT,
  [Skills.Athletics]: Stats.STR,
  [Skills.Deception]: Stats.CHA,
  [Skills.History]: Stats.INT,
  [Skills.Insight]: Stats.WIS,
  [Skills.Intimidation]: Stats.CHA,
  [Skills.Investigation]: Stats.INT,
  [Skills.Medicine]: Stats.WIS,
  [Skills.Nature]: Stats.INT,
  [Skills.Perception]: Stats.WIS,
  [Skills.Performance]: Stats.CHA,
  [Skills.Persuasion]: Stats.CHA,
  [Skills.Religion]: Stats.INT,
  [Skills.SleightOfHand]: Stats.DEX,
  [Skills.Stealth]: Stats.DEX,
  [Skills.Survival]: Stats.WIS,
}

export const Races = {
  Dragonborn: "Dragonborn",
  Dwarf: "Dwarf",
  Elf: "Elf",
  Gnome: "Gnome",
  HalfElf: "Half-Elf",
  Halfling: "Halfling",
  HalfOrc: "Half-Orc",
  Human: "Human",
  Tiefling: "Tiefling",
}

export const RaceFeatures = {
  [Races.Dragonborn]: {
    statChanges: {
      [Stats.STR]: 2,
      [Stats.DEX]: 0,
      [Stats.CON]: 0,
      [Stats.INT]: 0,
      [Stats.WIS]: 0,
      [Stats.CHA]: 1,
    },
    size: Sizes.Medium,
    speed: 30,
  },
  [Races.Dwarf]: {
    statChanges: {
      [Stats.STR]: 0,
      [Stats.DEX]: 0,
      [Stats.CON]: 2,
      [Stats.INT]: 0,
      [Stats.WIS]: 1,
      [Stats.CHA]: 0,
    },
    size: Sizes.Medium,
    speed: 25,
  },
  [Races.Elf]: {
    statChanges: {
      [Stats.STR]: 0,
      [Stats.DEX]: 2,
      [Stats.CON]: 0,
      [Stats.INT]: 1,
      [Stats.WIS]: 0,
      [Stats.CHA]: 0,
    },
    size: Sizes.MEDIUM,
    speed: 30,
  },
  [Races.Gnome]: {
    statChanges: {
      [Stats.STR]: 0,
      [Stats.DEX]: 0,
      [Stats.CON]: 1,
      [Stats.INT]: 2,
      [Stats.WIS]: 0,
      [Stats.CHA]: 0,
    },
    size: Sizes.Small,
    speed: 25,
  },
  [Races.HalfElf]: /*include choice for 2 extra points*/ {
    statChanges: {
      [Stats.STR]: 0,
      [Stats.DEX]: 0,
      [Stats.CON]: 0,
      [Stats.INT]: 0,
      [Stats.WIS]: 0,
      [Stats.CHA]: 2,
    },
    size: Sizes.Medium,
    speed: 30,
  },
  [Races.Halfling]: {
    statChanges: {
      [Stats.STR]: 0,
      [Stats.DEX]: 2,
      [Stats.CON]: 0,
      [Stats.INT]: 0,
      [Stats.WIS]: 0,
      [Stats.CHA]: 1,
    },
    size: Sizes.Small,
    speed: 25,
  },
  [Races.HalfOrc]: {
    statChanges: {
      [Stats.STR]: 2,
      [Stats.DEX]: 0,
      [Stats.CON]: 1,
      [Stats.INT]: 0,
      [Stats.WIS]: 0,
      [Stats.CHA]: 0,
    },
    size: Sizes.Medium,
    speed: 30,
  },
  [Races.Human]: {
    statChanges: {
      [Stats.STR]: 1,
      [Stats.DEX]: 1,
      [Stats.CON]: 1,
      [Stats.INT]: 1,
      [Stats.WIS]: 1,
      [Stats.CHA]: 1,
    },
    size: Sizes.Medium,
    speed: 30,
  },
  [Races.Tiefling]: {
    statChanges: {
      [Stats.STR]: 0,
      [Stats.DEX]: 0,
      [Stats.CON]: 0,
      [Stats.INT]: 1,
      [Stats.WIS]: 0,
      [Stats.CHA]: 2,
    },
    size: Sizes.Medium,
    speed: 30,
  },
}

export const Languages = {
  Common: "Common",
  Dwarvish: "dwarvish",
  Elvish: "elvish",
  Giant: "giant",
  Gnomish: "gnomish",
  Goblin: "goblin",
  Halfling: "halfling",
  Orc: "orc",

  Abyssal: "abyssal",
  Celestial: "celestial",
  Draconic: "draconic",
  Deep_Speech: "deep speech",
  Infernal: "infernal",
  Primordial: "primordial",
  Sylvan: "sylvan",
  Undercommon: "undercommon"
}

export const Tools = {
  Alchemists_Supplies: "alchemist's supplies",
  Brewers_Supplies: "brewer's supplies",
  Calligraphers_Tools: "calligrapher's tools",
  Carpenters_Tools: "carpenter's tools",
  Cobblers_Tools: "cobbler's Tools",
  Cooks_Utensils: "cook's utensils",
  Glassblowers_Tools: "glassblower's tools",
  Jewelers_Tools: "jeweler's tools",
  Leatherworkers_Tools: "leatherworker's tools",
  Masons_Tools: "mason's tools",
  Painters_Supplies: "painter's supplies",
  Potters_tools: "potter's tools",
  Smiths_Tools: "potter's tools",
  Tinkers_Tools: "tinker's tools",
  Weavers_Tools: "weaver's tools",
  Woodcarvers_Tools: "woodcarvers tools",

  Forgery_Kit: "forgery kit",
  Disguise_Kit: "disguise kit",
  Thieves_Tools: "thieves' tools",
  Herbalism_Kit: "herbalism_kit",
  Navigators_Tools: "navigator's tools",

  Cards: "cards",
  Dice: "dice",

  Land_Vehicles: "land vehichles",
  Water_Vechicles: "water vehicles"
}

export const Classes = {
  Barbarian: "Barbarian",
  Bard: "Bard",
  Cleric: "Cleric",
  Druid: "Druid",
  Fighter: "Fighter",
  Monk: "Monk",
  Paladin: "Paladin",
  Ranger: "Ranger",
  Rogue: "Rogue",
  Sorcerer: "Sorcerer",
  Warlock: "Warlock",
  Wizard: "Wizard",
}

export const ClassFeatures = {
  [Classes.Barbarian]: {
    hp: 12,
    savingThrows: [Stats.STR, Stats.CON],
    skills: {
      amount: 2,
      list: [Skills.AnimalHandling, Skills.Athletics, Skills.Intimidation, Skills.Nature, Skills.Perception, Skills.Survival]
    },
    gold: 5,
    equipment: [EquipmentList.Greataxe, EquipmentList.Handaxe, EquipmentList.Explorers_Pack],
    proficiency: 2,
    feat: [FeatureList.Rage,FeatureList.Unarmored_Defense]
  },
  [Classes.Bard]: {
    hp: 8,
    savingThrows: [Stats.DEX, Stats.CHA],
    skills: {
      amount: 3,
      list: ["All"] // get back to this and add all skills
    },
    gold: 12.5,
    equipment: [EquipmentList.Rapier, EquipmentList.Entertainers_Pack, EquipmentList.Lute, EquipmentList.Leather_Armor, EquipmentList.Dagger],
    proficiency: 2,
    feat: [FeatureList.Spellcasting, FeatureList.Bardic_Inspiration]
  },
  [Classes.Cleric]: {
    hp: 8,
    savingThrows: [Stats.WIS, Stats.CHA],
    skills: {
      amount: 2,
      list: [Skills.History, Skills.Insight, Skills.Medicine, Skills.Persuasion, Skills.Religion]
    },
    gold: 12.5,
    equipment: [EquipmentList.Mace, EquipmentList.Scale_Mail, EquipmentList.Light_Crossbow, EquipmentList.Priests_Pack, EquipmentList.Shield, EquipmentList.Holy_Symbol],
    proficiency: 2,
    feat: [FeatureList.Spellcasting, FeatureList.Divine_Domain]
  },
  [Classes.Druid]: {
    hp: 8,
    savingThrows: [Stats.INT, Stats.WIS],
    skills: {
      amount: 2,
      list: [Skills.Arcana, Skills.AnimalHandling, Skills.Insight, Skills.Medicine, Skills.Nature, Skills.Perception, Skills.Religion, Skills.Survival]
    },
    gold: 5,
    equipment: [EquipmentList.Shield, EquipmentList.Scimitar, EquipmentList.Leather_Armor, EquipmentList.Explorers_Pack, EquipmentList.Druidic_Focus],
    proficiency: 2,
    feat: [FeatureList.Druidic, FeatureList.Spellcasting]
  },
  [Classes.Fighter]: {
    hp: 10,
    savingThrows: [Stats.STR, Stats.CON],
    skills: {
      amount: 2,
      list: [Skills.Acrobatics, Skills.AnimalHandling, Skills.Athletics, Skills.History, Skills.Insight, Skills.Intimidation, Skills.Perception, Skills.Survival]
    },
    gold: 12.5,
    equipment: [EquipmentList.Chain_Mail, EquipmentList.Longsword, EquipmentList.Shield, EquipmentList.Light_Crossbow, EquipmentList.Dungeoneers_Pack],
    proficiency: 2,
    feat: [FeatureList.Fighting_Style,FeatureList.Second_Wind]
  },
  [Classes.Monk]: {
    hp: 8,
    savingThrows: [Stats.STR, Stats.DEX],
    skills: {
      amount: 2,
      list: [Skills.Acrobatics, Skills.Athletics, Skills.History, Skills.Insight, Skills.Religion, Skills.Stealth]
    },
    gold: 1.3,
    equipment: [EquipmentList.shortsword, EquipmentList.Explorers_Pack],
    proficiency: 2,
    feat: [FeatureList.Unarmored_Defense,FeatureList.Martial_Arts]
  },
  [Classes.Paladin]: {
    hp: 10,
    savingThrows: [Stats.WIS, Stats.CHA],
    skills: {
      amount: 2,
      list: [Skills.Athletics, Skills.Insight, Skills.Intimidation, Skills.Medicine, Skills.Persuasion, Skills.Religion]
    },
    gold: 12.5,
    equipment: [EquipmentList.Longsword, EquipmentList.Shield, EquipmentList.Javelin, EquipmentList.Priests_Pack, EquipmentList.Chain_Mail, EquipmentList.Holy_Symbol],
    proficiency: 2,
    feat: [FeatureList.Divine_Sense,FeatureList.Lay_on_Hands]
  },
  [Classes.Ranger]: {
    hp: 10,
    savingThrows: [Stats.STR, Stats.DEX],
    skills: {
      amount: 3,
      list: [Skills.AnimalHandling, Skills.Athletics, Skills.Insight, Skills.Investigation, Skills.Nature, Skills.Perception, Skills.Stealth, Skills.Survival]
    },
    gold: 12.5,
    equipment: [EquipmentList.Scale_Mail, EquipmentList.shortsword, EquipmentList.shortsword, EquipmentList.Dungeoneers_Pack, EquipmentList.Longbow, EquipmentList.Arrows],
    proficiency: 2,
    feat: [FeatureList.Favored_Enemy, FeatureList.Natural_Explorer]
  },
  [Classes.Rogue]: {
    hp: 8,
    savingThrows: [Stats.DEX, Stats.INT],
    skills: {
      amount: 4,
      list: [Skills.Acrobatics, Skills.Athletics, Skills.Deception, Skills.Insight, Skills.Intimidation, Skills.Investigation, 
        Skills.Perception, Skills.Performance, Skills.Persuasion, Skills.SleightOfHand, Skills.Stealth]
    },
    gold: 10,
    equipment: [EquipmentList.Rapier, EquipmentList.Shortbow, EquipmentList.Burglars_Pack, EquipmentList.Leather_Armor, EquipmentList.Dagger, EquipmentList.Dagger, EquipmentList.Thieves_Tools],
    proficiency: 2,
    feat: [FeatureList.Expertise, FeatureList.Sneak_Attack]
  },
  [Classes.Sorcerer]: {
    hp: 6,
    savingThrows: [Stats.CON, Stats.CHA],
    skills: {
      amount: 2,
      list: [Skills.Arcana, Skills.Deception, Skills.Insight, Skills.Intimidation, Skills.Persuasion, Skills.Religion]
    },
    gold: 7.5,
    equipment: [EquipmentList.Light_Crossbow, EquipmentList.Crossbow_Bolts, EquipmentList.Component_Pouch, EquipmentList.Dungeoneers_Pack, EquipmentList.Dagger, EquipmentList.Dagger],
    proficiency: 2,
    feat: [FeatureList.Spellcasting, FeatureList.Sourcerous_Origin]
  },
 
  [Classes.Warlock]: {
    hp: 8,
    savingThrows: [Stats.WIS, Stats.CHA],
    skills: {
      amount: 2,
      list: [Skills.Arcana, Skills.Deception, Skills.History, Skills.Intimidation, Skills.Investigation, Skills.Nature, Skills.Religion]
    },
    gold: 10,
    equipment: [EquipmentList.Light_Crossbow, EquipmentList.Crossbow_Bolts, EquipmentList.Component_Pouch, EquipmentList.Scholars_Pack, EquipmentList.Leather_Armor, 
      EquipmentList.shortsword, EquipmentList.Dagger, EquipmentList.Dagger],
    proficiency: 2,
    feat: [FeatureList.Otherworldly_Patron, FeatureList.Pact_Magic]
  },
  [Classes.Wizard]: {
    hp: 6,
    savingThrows: [Stats.INT, Stats.WIS],
    skills: {
      amount: 2,
      list: [Skills.Arcana, Skills.History, Skills.Insight, Skills.Investigation, Skills.Medicine, Skills.Religion]
    },
    gold: 10,
    equipment: [EquipmentList.Quarterstaff, EquipmentList.Arcane_Focus, EquipmentList.Scholars_Pack, EquipmentList.Spellbook],
    proficiency: 2,
    feat: [FeatureList.Spellcasting, FeatureList.Arcane_Recovery]
  },
}

export const Backgrounds = {
  Acolyte: "Acolyte",
  Charlatan: "Charlatan",
  Criminal: "Criminal",
  Spy: "Spy",
  Entertainer: "Entertainer",
  Gladiator: "Gladiator",
  Folk_Hero: "Folk hero",
  Guild_Artisan: "Guild Artisan",
  Guild_Merchant: "Guild Merchant",
  Hermit: "Hermit",
  Noble: "Noble",
  Knight: "Knight",
  Outlander: "Outlander",
  Sage: "Sage",
  Sailor: "Sailor",
  Pirate: "Pirate",
  Soldier: "Soldier",
  Urchin: "Urchin",
}

export const BackgroundFeatures = {
  [Backgrounds.Acolyte]: {
    skills: {
      amount: 2,
      list: [Skills.Deception, Skills.SleightOfHand],
    },
    languages: {
      amount: 2,
      list: [Languages.Common, Languages.Dwarvish,Languages.Elvish,Languages.Abyssal,Languages.Celestial,Languages.Deep_Speech,Languages.Draconic,Languages.Giant,Languages.Gnomish,Languages.Goblin,Languages.Halfling,Languages.Infernal,Languages.Orc,Languages.Primordial,Languages.Sylvan,Languages.Undercommon],
    },
    equipment: [EquipmentList.Holy_Symbol, EquipmentList.Prayer_Book, EquipmentList.Incense, EquipmentList.Vestiments, EquipmentList.Common_Clothes, EquipmentList.Belt_Pouch],
    gold:15,
    feat: [FeatureList.Shelter_the_Faithful]
  },
  [Backgrounds.Charlatan]: {
    skills: {
      amount: 2,
      list: [Skills.Insight, Skills.Religion],
    },
    tools: {
      amount: 2,
      list: [Tools.Disguise_Kit, Tools.Forgery_Kit],
    },
    equipment: [EquipmentList.Fine_Clothes, EquipmentList.Disguise_Kit, EquipmentList.Bottles_of_Liquid, EquipmentList.Belt_Pouch],
    gold: 15,
    feat: [FeatureList.Favorite_Scheme, FeatureList.False_Identity]
  },
  [Backgrounds.Criminal]: {
    skills: {
      amount: 2,
      list: [Skills.Deception, Skills.Stealth],
    },
    tools: {
      amount: 2,
      list: [Tools.Dice, Tools.Cards, Tools.Thieves_Tools],
    },
    equipment: [EquipmentList.Crowbar, EquipmentList.Dark_clothes, EquipmentList.Belt_Pouch],
    gold: 15,
    feat: [FeatureList.Criminal_Specialty, FeatureList.Criminal_Contact]
  },
  [Backgrounds.Spy]: {
    skills: {
      amount: 2,
      list: [Skills.Deception, Skills.Stealth],
    },
    tools: {
      amount: 2,
      list: [Tools.Dice, Tools.Cards, Tools.Thieves_Tools],
    },
    equipment: [EquipmentList.Crowbar, EquipmentList.Dark_clothes, EquipmentList.Belt_Pouch],
    gold: 15,
    feat: [FeatureList.Criminal_Specialty, FeatureList.Criminal_Contact]
  },
  [Backgrounds.Entertainer]: {
    skills: {
      amount: 2,
      list: [Skills.Acrobatics, Skills.Performance],
    },
    tools: {
      amount: 2,
      list: [Tools.Disguise_Kit, Tools.Lyre],
    },
    equipment: [EquipmentList.Lyre, EquipmentList.Trinket, EquipmentList.Costume, EquipmentList.Belt_Pouch],
    gold: 15,
    feat: [FeatureList.Entertainer_Routines, FeatureList.By_Popular_Demand]
  },
  [Backgrounds.Gladiator]: {
    skills: {
      amount: 2,
      list: [Skills.Acrobatics, Skills.Performance],
    },
    tools: {
      amount: 2,
      list: [Tools.Disguise_Kit, Tools.Lyre],
    },
    equipment: [EquipmentList.Net, EquipmentList.Trinket, EquipmentList.Costume, EquipmentList.Belt_Pouch],
    gold: 15,
    feat: [FeatureList.Entertainer_Routines, FeatureList.By_Popular_Demand]
  },
  [Backgrounds.Folk_Hero]: {
    skills: {
      amount: 2,
      list: [Skills.AnimalHandling, Skills.Survival],
    },
    tools: {
      amount: 2,
      list: [Tools.Masons_Tools, Tools.Land_Vehicles],
    },
    equipment: [EquipmentList.Masons_Tools, EquipmentList.Shovel, EquipmentList.Iron_Pot, EquipmentList.Common_Clothes, EquipmentList.Belt_Pouch],
    gold: 10,
    feat: [FeatureList.Defining_Event,FeatureList.Rustic_Hospitality]
  },
}

export function calculateHitPoints(class_, race, con) {
  let classValue = ClassFeatures[class_] ? ClassFeatures[class_].hp : 0;
  let raceValue = RaceFeatures[race] ? calculateStatBonus(RaceFeatures[race].statChanges[Stats.CON] + 10) : 0;
  let statValue = calculateStatBonus(con);

  let hp = classValue + raceValue + statValue;

  return hp;
}

export function calculateStatBonus(value) {
  var bonus = value - 10;
  bonus = Math.floor(bonus / 2);
  return bonus;
}

export function getSign(value) {
  return value >= 0 ? "+" : "";
}