import React, { Fragment } from 'react';
import Expand from "react-expand-animated";

class Expander extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        open: (props.open === "true") ? true : false,
      };
    }
  
    toggle = () => {
      this.setState({ open: !this.state.open })
    }
  
    render() {
      const transitions = ["height", "opacity", "background"];
  
      return (
        <Fragment>
          <div className="expander">
            <div className="expander-bar">
              <button onClick={this.toggle}>
                { this.state.open ? 'v' : '>' } 
              </button>
              <h2 className="expander-title">{this.props.title}</h2>
            </div>
            <Expand
              open={this.state.open}
              duration={200}
              transitions={transitions}
            >
              <div className="expander-children">
                {this.props.children}
              </div>
            </Expand>
          </div>
        </Fragment>
      );
    }
  }

  export default Expander;
