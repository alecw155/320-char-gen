import React, {useState} from 'react';
import { SubMenuItems } from './MenuItems';
import { Link } from 'react-router-dom';
import './Dropdown.css';
//import { CSSTransition } from 'react-transition-group';

function Dropdown() {

    const [click, setClick] = useState(false);

    const handleClick = () => setClick(!click);

    return (
        //<CSSTransition 
        //in={!click}
        //timeout={350}
        //classNames="display"
       // >
            <ul
            onClick={handleClick}
            className={click ? 'dropmenu clicked' : 'dropmenu' }
            >
                {SubMenuItems.map((item, index) => {
                    return(
                        <li key={index}>
                            <Link 
                            className={item.cName}
                            to={item.url}
                            onClick={() => setClick(false)}
                            >
                                {item.title}
                            </Link>
                        </li>
                    );
                })}
            </ul>
       // </CSSTransition>
    );
}

export default Dropdown;