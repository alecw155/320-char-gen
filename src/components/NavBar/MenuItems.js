export const SubMenuItems = [
    {
        title: 'New Character',
        url: '/new_char',
        cName: 'dropdown_link'
    },
    {
        title: 'Recover Character',
        url: '/recov_char',
        cName: 'dropdown_link'
    },
    {
        title: 'Print Character',
        url: '/print_char',
        cName: 'dropdown_link'
    },
]