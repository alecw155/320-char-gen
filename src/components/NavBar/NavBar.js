import React, { useState } from 'react';
import Logo from "../images/logo.png";
import { Link } from 'react-router-dom';
import './NavBar.css';
import Dropdown from './Dropdown';


// =====================================
  function NavBar() {
      //setClick will set click
      const [click, setClick] = useState(false);
      // Drop down menu for creator
      const [dropdown, setDropdown] = useState(false);
      //Sets click to true or false
      const handleClick = () => setClick(!click);
      // When clicking an item it will close the menu without clicking the X icon
      const closeMobileMenu = () => setClick(false);
      // Will check if the user is on a windows device or not. Then will activate Dropdown.
      const onMouseEnter = () => {
        window.innerWidth < 960 ? setDropdown(false) : setDropdown(true);
      };
      // Close the dropdown menu
      const onMouseLeave = () => {
        window.innerWidth < 960 ? setDropdown(false) : setDropdown(false);
      };

      return ( 
          <nav className="Navbar">
            <Link to='/'>
              <img src={Logo} alt="Logo"></img>
            </Link>
            <Link to='/' className='navbar_logo'>
              D&D Character Creator
            </Link>
            {/* Incoperating a hamberger menu when in mobile*/}
            <div className='menu_icon' onClick={handleClick}>
              {/*If the menu_icon is clicked then it will show a X else the three bars*/}
              <i className={click ? 'fas fa-times': 'fas fa-bars'} />
            </div>
            <ul className={click ? 'menu active' : 'menu'}>
              
              <li className='nav_item'>
                <Link 
                to ='/' 
                className={click ? 'nav_link active' : 'nav_link'}
                onClick={closeMobileMenu}>
                Home
                </Link>
              </li>

              {/*Using onMouseEnter/Leave will allow the menu to pop up when ever the mouse hovers the button*/}
              <li 
              className='nav_item'
              onMouseEnter={onMouseEnter}
              onMouseLeave={onMouseLeave}
              >
                <Link 
                to ='/new_char' 
                className={click ? 'nav_link active' : 'nav_link'} 
                onClick={closeMobileMenu}>
                Creator <i className={dropdown ? 'fas fa-caret-up' : 'fas fa-caret-down'} />
                </Link>
                {dropdown && <Dropdown />}
              </li>

              <li className='nav_item'>
                <Link 
                to ='/about' 
                className={click ? 'nav_link active' : 'nav_link'}
                onClick={closeMobileMenu}>
                About
                </Link>
              </li>

            </ul>
          </nav>
      );

  }//class Navbar()
// ================

export default NavBar