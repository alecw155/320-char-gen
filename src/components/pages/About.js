import React from 'react';
import './About.css';

export default function About() {
    return (

      <div className='About'>
        <div className='Background-About' />
        <h1 className='Title-About'>About</h1>
        <div className='body-about'>
          
          <p className='p-about'>
            Welcome to Dungeons and Dragons 5e Character Creator.
            This character creator was made using React JS and contains only public open source materials.
            The character creator allows you to customize all aspects of your character using drop down menus
            and then export your completed character as a 5e character sheet in  HTML or as JSON format.
          </p>

          <p className='p-about'>
            This project was made as part of the WSUV CS 320 programing project 2020 and made by Alec Williams,
            Austin Reyes, Nathaniel Adams, and Trevor Holland.
          </p>

          <p className='p-about'>
            Credit goes to Ben Morton for allowing us to use their "dnd-5e-srd" github repo, nevertras on Reddit for
            letting us use their ”5e character sheet with pure HTML/CSS", backgrounds are credited to James' RPG from youtube and Wizards of the Coast for making such a great game!
          </p>

          <div className='links-about'>
            <a className='link-about' 
              href="https://github.com/BTMorton/dnd-5e-srd" 
              target="_blank" 
              rel="noopener noreferrer"
              title="Ben Morton github">
                <i className='fab fa-github'/>
            </a>
            <a className='link-about' 
              href="https://www.reddit.com/r/dndnext/comments/6b8gv5/5e_character_sheet_with_pure_htmlcss/" 
              target="_blank" 
              rel="noopener noreferrer"
              title="Reddit 5e character sheet">
                <i className='fab fa-reddit'/>
            </a>
            <a className='link-about'
              href="https://www.youtube.com/channel/UCJifhbm-Xqen0RqHtERe-AQ" 
              target="_blank" 
              rel="noopener noreferrer"
              title="James' RPG Background"> 
              <i className='fab fa-youtube' />
            </a>
            <a className='link-about' 
              href = "https://dnd.wizards.com/" 
              target="_blank" 
              rel="noopener noreferrer"
              title="Wizards of the Coast DND">
                <i className='fab fa-d-and-d-beyond'/>
            </a>
          </div>

        </div>
      </div>
    );
}