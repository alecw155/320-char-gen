import React from 'react';
import { Link } from 'react-router-dom';
import './Data_Present.css'

export default function Data_Present(localData) {
    let Player = JSON.parse(localData);
    
    return(
        <div className="Data-present">
            <h className="Data-Present-h1">Would you like to overwrite character: {Player.name}</h>
            <div className="Data-Present-Buttons">
                <button 
                onClick={() => {
                    localStorage.clear();
                    window.location.reload();
                }} 
                className="Data-Present-Button">
                    Yes
                </button> 
                <Link to='/recov_char' className='to-recov'>
                    <button className="Data-Present-Button">
                        No
                    </button>
                </Link>
            </div>
        </div>
    );
}