import React, { useState } from 'react';
import './Home.css';


export default function Home() {

    const [click, setClick] = useState(false);

    const handleClick = () => setClick(!click);

    return (
        <div className='Home'>
            <div className='Background-Home'/>
            <div className='Text-Header-Home' />
            <h1 className='Title-home'>Dungeons and Dragons</h1>
            <h2 className='Sub-Title-home'> Character Creator</h2>
            <div className={click ? 'Scroll-body-home active' : 'Scroll-body-home'} onClick={handleClick}>
                <i className={click ? 'fas fa-chevron-down' : 'fas fa-chevron-up'} />
                <div className={click ? 'info-home active' : 'info-home'} >
                        <p className="p1-home">
                            Dungeons and Dragons 5e Character Creator is a a complete character creator for
                            Dungeons and Dragons 5e using Open source materials. Using this character creator
                            you will be able to choose the parameters of your character such as race or class
                            and the creator will then import your character's traits and features automatically
                            to a character sheet.When you have finished your character you may also export your
                            completed character sheet for later use.
                        </p>

                        <p className="p2-home">
                            Hover over the Creator tab and click "New Character" to get started. Click "Recover Character"
                            to come back to a previously created character. Click "Print Character" to print out your character.
                            Click on the About tab to learn more about the team behind the project.
                        </p>
                </div>
            </div>
        </div>
    );
}