import React from 'react';
import Sheet from '../Character Creator/Sheet';
import Data_Present from '../pages/Data_Present'

export default function New_char() {
    // Will check if localstorage is present for character.
    const localData = localStorage.getItem('character');

    if (localData != null) {
        return (Data_Present(localData));
    }
    return (<Sheet />);
}