import React from 'react';
import Sheet from '../Character Creator/Sheet';
import './Recov_char.css';

export default function Recov_char() {

    const localData = localStorage.getItem('character');

    if (localData != null) {
        return ( 
            <Sheet />
        );
    }
    return (
      <h className="recov-char-h1">
          ERROR: No character present!
      </h>  
    );
    
}